# GP Drawing view

operator that toggles view and mode to drawing oriented presets. It feels like entering a symbol in Adobe Animate, switching automatically from Object to Draw mode

# [Download latest as zip](https://gitlab.com/andarta.pictures/blender/gp-drawing-view/-/archive/master/gp-drawing-view-master.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list

# User guide

- First assign a shortcut to the operator in the addon preferences :

![guide](docs/gpdrawview_02.png "guide")

- In object mode, select the grease pencil object you want to work on, then hit the shortcut you assigned.

- Local draw view : The view is now centered and parented on the selected object, blender turned into draw mode and fade object option is enabled in the overlay settings.  You can get out of this mode by hitting again the shortcut assigned, or either ESC or MMB. Blender will turn back to object mode and the view should return exactly where you were previously. 

- Simple draw view : The point of view stays the same but blender turned into draw mode and fade object option is enabled in the overlay settings. Hitting the shortcut key will turn the overlay settings back to their previous state and switch back to object mode.

# Warning

This tool is in early development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)

