# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "GP_Draw_view",
    "author" : "Tom VIGUIER",
    "description" : "operator that toggles view and mode to drawing oriented presets",
    "blender" : (2, 91, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Andarta"
}


import bpy
import math
from mathutils import *
from bpy.props import (StringProperty, IntProperty, BoolProperty, PointerProperty)

from bpy.types import (Panel,
                       Operator,
                       AddonPreferences,
                       PropertyGroup,
                       )


# this is an internal API at the moment
import rna_keymap_ui


class GP_DV_AddonPref(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __name__
    '''
    filepath : StringProperty(
            name="Example File Path",
            subtype='FILE_PATH',
            )
    number : IntProperty(
            name="Example Number",
            default=4,
            )
    boolean : BoolProperty(
            name="Example Boolean",
            default=False,
            )
    '''
    def draw(self, context):
        layout = self.layout
        layout.label(text="KeyMap :")
        #layout.prop(self, "filepath")
        #row = layout.row()
        #row.prop(self, "number")
        #row.prop(self, "boolean")

        col = layout.column()
        kc = bpy.context.window_manager.keyconfigs.addon
        for km, kmi in addon_keymaps:
            km = km.active()
            col.context_pointer_set("keymap", km)
            rna_keymap_ui.draw_kmi([], kc, km, kmi, col, 0)






class DV_PROPS(PropertyGroup):
    bl_label = "Drawing view properties"
    bl_idname = "dv.properties"

    switch_state : bpy.props.BoolProperty(default = False)
    switch_2_state : bpy.props.BoolProperty(default = False)
    init_view_matrix : bpy.props.FloatVectorProperty(
    name="Matrix",
    size=16,
    subtype="MATRIX")

    init_location : bpy.props.FloatVectorProperty()
    init_perspective : bpy.props.StringProperty() 
    init_camera_zoom : bpy.props.FloatProperty()
    init_camera_obj : bpy.props.PointerProperty(type = bpy.types.Object)
    init_camera_offset : bpy.props.FloatVectorProperty(size = 2)
    
    last_camera_offset : bpy.props.FloatVectorProperty(size = 2)
    last_camera_zoom : bpy.props.FloatProperty()
    last_obj : bpy.props.PointerProperty(type = bpy.types.Object)
    
    ugfo2 : bpy.props.BoolProperty(default = False)
    ugfgpo2 : bpy.props.BoolProperty(default = False)
    osif2 : bpy.props.BoolProperty(default = False)


class DV_OT_SWITCH_2(bpy.types.Operator) :
    bl_label = "switch to simple draw view (no reframing)"
    bl_idname = 'gpencil.dvswitch_2'
    #bl_options = {'REGISTER','UNDO'}
    ##switch_state : bpy.props.BoolProperty(default = False)
    cam_name : bpy.props.StringProperty()
    ugfo : bpy.props.BoolProperty(default = False)
    ugfgpo : bpy.props.BoolProperty(default = False)
    osif : bpy.props.BoolProperty(default = False)

    def execute(self, context):
        ob = bpy.context.object
        dv_data = bpy.context.scene.dv_data
        if ob is not None :
            if ob.type == 'GPENCIL' :
                if dv_data.switch_2_state == False :
                    self.switch_on()
                    
                    return{'FINISHED'}
                if dv_data.switch_2_state == True :
                    self.switch_off()
                    
                    return{'FINISHED'}
            else :
                return {'CANCELLED'}
        else :
            return {'CANCELLED'}
        
    def switch_on(self):
        ob = bpy.context.object
        dv_data = bpy.context.scene.dv_data
        
        #view settings
        dv_data.osif2 = ob.show_in_front
        ob.show_in_front = True
        for area in bpy.context.screen.areas :
            if area.type == 'VIEW_3D' :                 
                overlay = area.spaces[0].overlay 
                dv_data.ugfo2 = overlay.use_gpencil_fade_objects
                dv_data.ugfgpo2 = overlay.use_gpencil_fade_gp_objects
                overlay.use_gpencil_fade_objects = True
                overlay.use_gpencil_fade_gp_objects = True
            
        ## switch to draw mode
        bpy.ops.object.mode_set(mode='PAINT_GPENCIL')
        dv_data.switch_2_state = True

    def switch_off(self):
        ob = bpy.context.object
        dv_data = bpy.context.scene.dv_data

        #view settings
        ob.show_in_front = dv_data.osif2
    
        for area in bpy.context.screen.areas :
            if area.type == 'VIEW_3D' :                 
                overlay = area.spaces[0].overlay 
                overlay.use_gpencil_fade_objects = dv_data.ugfo2 
                overlay.use_gpencil_fade_gp_objects = dv_data.ugfgpo2  

            
        ## switch to object mode
        bpy.ops.object.mode_set(mode='OBJECT')


        dv_data.switch_2_state = False


class DV_OT_SWITCH(bpy.types.Operator) :
    bl_label = "switch to local draw view"
    bl_idname = 'gpencil.dvswitch'
    #bl_options = {'REGISTER','UNDO'}
    #switch_state : bpy.props.BoolProperty(default = False)
    cam_name : bpy.props.StringProperty()
    ugfo : bpy.props.BoolProperty(default = False)
    ugfgpo : bpy.props.BoolProperty(default = False)
    osif : bpy.props.BoolProperty(default = False)

    def modal(self, context, event):
        
        if event.type == 'MIDDLEMOUSE' and not event.alt and not event.ctrl and not event.shift :
            self.switch_off()
            bpy.context.scene.dv_data.switch_state = False
            return {'FINISHED'}

        elif event.type == 'ESC' :
            self.switch_off()
            bpy.context.scene.dv_data.switch_state = False
            return {'FINISHED'}

        elif event.type == bpy.context.window_manager.keyconfigs.addon.keymaps['3D View'].keymap_items['gpencil.dvswitch'].type :
            if event.value == 'PRESS' :
                self.switch_off()
                bpy.context.scene.dv_data.switch_state = False
                return {'FINISHED'}
        
        else :
            return {'PASS_THROUGH'}

        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        
        ob = bpy.context.object
        dv_data = bpy.context.scene.dv_data
        if ob is not None :
            if ob.type == 'GPENCIL' :
                if dv_data.switch_state == False :

                    self.switch_on()

                    context.window_manager.modal_handler_add(self)
                    return {'RUNNING_MODAL'}
                if dv_data.switch_state == True :
                    return {'CANCELLED'}
            else :
                print('select a GP')
                return {'CANCELLED'}
        else :
            print('select a GP')
            return {'CANCELLED'}
    
    def switch_off(self) :
        ob = bpy.context.object
        dv_data = bpy.context.scene.dv_data

        if self.cam_name is not None:
            #print(self.cam_name)
            for camera in bpy.data.cameras :
                if camera.name == self.cam_name :
                    bpy.data.cameras.remove(camera)
        bpy.ops.object.mode_set(mode='OBJECT')
        for collection in bpy.data.collections :
            if collection.name.startswith('Drawing_view'):
                bpy.data.collections.remove(collection)
                
        ## set overlay setting back
        ob.show_in_front = self.osif
        for area in bpy.context.screen.areas :
                if area.type == 'VIEW_3D' :
                    area.spaces[0].region_3d.view_perspective = 'CAMERA'
                    overlay = area.spaces[0].overlay 
                    overlay.use_gpencil_fade_objects = self.ugfo
                    overlay.use_gpencil_fade_gp_objects = self.ugfgpo 
        
                    #store last view settings
                    dv_data.last_camera_offset = area.spaces[0].region_3d.view_camera_offset
                    dv_data.last_camera_zoom = area.spaces[0].region_3d.view_camera_zoom
                    dv_data.last_obj = ob
                                    
                    
                    #restore initial viewpoint 
                    
                    area.spaces[0].region_3d.view_matrix = Matrix(dv_data.init_view_matrix)
                    
                    area.spaces[0].region_3d.view_location = dv_data.init_location
                    area.spaces[0].region_3d.view_perspective = dv_data.init_perspective
                    if dv_data.init_perspective == 'CAMERA':
                        area.spaces[0].region_3d.view_perspective = dv_data.init_perspective
                        bpy.context.scene.camera = dv_data.init_camera_obj 
                        area.spaces[0].region_3d.view_camera_zoom = dv_data.init_camera_zoom 
                        area.spaces[0].region_3d.view_camera_offset = dv_data.init_camera_offset
                            
        ##finish
        dv_data.switch_state = False

    def flatten(self, mat):
        dim = len(mat)
        return [mat[j][i] for i in range(dim) 
                        for j in range(dim)]        
        
        
    def switch_on(self) :
        ob = bpy.context.object
        scene = bpy.context.scene
        dv_data = bpy.context.scene.dv_data
      
        if ob.type == 'GPENCIL' :
            dv_collection = bpy.data.collections.new('Drawing_view')
            scene.collection.children.link(dv_collection)
            gp_ob = ob.data

            #store initial viewpoint
            for area in bpy.context.screen.areas :
                if area.type == 'VIEW_3D' :
                    
                    dv_data.init_view_matrix = self.flatten(area.spaces[0].region_3d.view_matrix)
                    dv_data.init_location = area.spaces[0].region_3d.view_location
                    dv_data.init_perspective = area.spaces[0].region_3d.view_perspective
                    if dv_data.init_perspective == 'CAMERA':
                        dv_data.init_camera_obj = bpy.context.scene.camera
                        dv_data.init_camera_zoom = area.spaces[0].region_3d.view_camera_zoom
                        dv_data.init_camera_offset = area.spaces[0].region_3d.view_camera_offset
                    
            
            dv_data.switch_state = True
            
            
            #print("obj = gpencil")
            
            ####create a plane fitting the zone
            ####-------------------------------
            
            
            
            #get the field size (extreme points)
            up = -1000000.0
            down = 1000000.0
            left = 1000000.0
            right = -1000000.0
            
            ob_matrix_world = Matrix(ob.matrix_world)
            gp_ob = ob.data
            #print('****', ob.name, ob.data.name)
            #print(ob_matrix_world)
            for layer in gp_ob.layers : 
                frame = layer.active_frame 
                for stroke in frame.strokes :
                    for point in stroke.points :
                        world_point_co = ob_matrix_world @ point.co

                        if point.co[0] > right :
                            right = point.co[0]
                        if point.co[0] < left :
                            left = point.co[0]
                        if point.co[2] > up :
                            up = point.co[2]
                        if point.co[2] < down :
                            down = point.co[2]
            #print(left, right, up, down)        
            #create vertices and faces   
                       
            myvertex = []
            myfaces = []

            mypoint = [(left, 0.0, up)] 
            myvertex.extend(mypoint)
            mypoint = [(left, 0.0, down)]
            myvertex.extend(mypoint)
            mypoint = [(right, 0.0, down)]
            myvertex.extend(mypoint)
            mypoint = [(right, 0.0, up)]
            myvertex.extend(mypoint)
            myface = [(0, 1, 2, 3)]
            myfaces.extend(myface)

            mymesh = bpy.data.meshes.new('DV_Plane')
            dv_plane = bpy.data.objects.new('DV_Plane', mymesh)
            dv_collection.objects.link(dv_plane) 
            
            dv_plane.hide_render = True

            # Generate mesh data
            mymesh.from_pydata(myvertex, [], myfaces)
            # Calculate the edges
            mymesh.update(calc_edges=True)

            # Set Location
            dv_plane.matrix_local = ob_matrix_world
            #dv_plane.matrix_world = ob_matrix_world
            #dv_plane.location -= new_origin

            #center origin
            plane_vertices_co = []
            for vertex in dv_plane.data.vertices :
                plane_vertices_co.append(vertex.co)

            new_origin = sum(plane_vertices_co, Vector()) / len(plane_vertices_co)
            dv_plane.data.transform(Matrix.Translation(-new_origin))
            #print(dv_plane.location)
            
            dv_plane.location += new_origin @ ob_matrix_world
            #print(dv_plane.location)

            #remove negative scaling
            for i in range(0,3) :
                if dv_plane.scale[i] < 0 :
                    dv_plane.scale = dv_plane.scale * -1
            #remove 180° due to negative scaling
            dv_plane.rotation_euler = ob.rotation_euler
            
            



            #####create a camera and add it to dv collection
            cam = bpy.data.cameras.new("DV_Camera")
            cam.show_passepartout = False
            cam_obj = bpy.data.objects.new("DV_Camera", cam)
            dv_collection.objects.link(cam_obj)
            
            self.cam_name = cam.name
            

            #define as current camera
            bpy.context.scene.camera = cam_obj
            
            empty_gp = True
            if len(gp_ob.layers) != 0 :
                for layer in gp_ob.layers :
                    if len(layer.frames) != 0 :
                        for frame in layer.frames :
                            if len(frame.strokes) != 0 :
                                for stroke in frame.strokes :
                                    if len(stroke.points) != 0 :
                                        empty_gp = False
                                        break

            if empty_gp :  
                print('empty gp')  
                cam_distance = -6.0
            else :    
                if dv_plane.dimensions[0] /dv_plane.dimensions[2] >= 16/9  :#horizontal
                    cam_distance = -(dv_plane.dimensions[0]/2)  / (math.tan((cam.angle/2) ))
                else:  #vertical                   
                    cam_distance = -(dv_plane.dimensions[2]/2)  / (math.tan((cam.angle*(9/16)/2) ))  

            bpy.context.view_layer.update()
            cam_obj.matrix_world = dv_plane.matrix_world
            
            
            cam_obj.matrix_world = cam_obj.matrix_world @ Matrix(((1.0, 0.0, 0.0, 0.0),
            (0.0, -4.371138828673793e-08, -1.0, cam_distance * 1.1),
            (0.0, 1.0, -4.371138828673793e-08, -2.185569428547751e-06),
            (0.0, 0.0, 0.0, 1.0))) 
            
            #print(cam_obj.matrix_world, ob.matrix_world)
            #cam_obj.rotation_euler = [1.5707963705062866,0,0]
            cam_obj.scale = [1,1,1] 
            cam.type = 'PERSP'


            #delete dv plane
            bpy.data.meshes.remove(dv_plane.data)
            


            #parent camera to object
            cam_obj.parent = ob
            cam_obj.matrix_parent_inverse = ob.matrix_world.inverted()
            

            #view settings
            self.osif = ob.show_in_front
            ob.show_in_front = True
            for area in bpy.context.screen.areas :
                if area.type == 'VIEW_3D' :
                    
                    area.spaces[0].region_3d.view_perspective = 'CAMERA'
                    
                    overlay = area.spaces[0].overlay 
                    self.ugfo = overlay.use_gpencil_fade_objects
                    self.ugfgpo = overlay.use_gpencil_fade_gp_objects
                    overlay.use_gpencil_fade_objects = True
                    overlay.use_gpencil_fade_gp_objects = True

                    if ob == dv_data.last_obj :
                        #use last view
                        area.spaces[0].region_3d.view_camera_offset = dv_data.last_camera_offset
                        area.spaces[0].region_3d.view_camera_zoom = dv_data.last_camera_zoom
                    
                    else :
                        #fit view
                        bpy.ops.view3D.view_center_camera()
                        #bpy.ops.view3D.zoom_camera_1_to_1()
                        
            
            ## switch to draw mode
            bpy.ops.object.mode_set(mode='PAINT_GPENCIL')
            
            dv_data.switch_state = True
            
            
addon_keymaps = []           

classes = [DV_OT_SWITCH,
    DV_PROPS,
    GP_DV_AddonPref,
    DV_OT_SWITCH_2,
    
    ]               
                        
def register():
    for cls in classes :
        bpy.utils.register_class(cls)
    
    bpy.types.Scene.dv_data = bpy.props.PointerProperty(type=DV_PROPS)
    
    # Keymapping
    kc = bpy.context.window_manager.keyconfigs.addon
    if kc :
        km = kc.keymaps.new(name="3D View", space_type='VIEW_3D')
        kmi = km.keymap_items.new("gpencil.dvswitch", type = 'QUOTE', value = 'PRESS', shift = True)
        kmi.active = True
        addon_keymaps.append((km, kmi))

        kmi = km.keymap_items.new("gpencil.dvswitch_2", type = 'QUOTE', value = 'PRESS')
        kmi.active = True
        addon_keymaps.append((km, kmi))
        #kmi = km.keymap_items.new("object.transform_apply", 'NUMPAD_SLASH', 'PRESS', shift=True)
        #kmi.active = True
        #addon_keymaps.append((km, kmi))

'''
    wm = bpy.context.window_manager
    kc = wm.keyconfigs.addon
    if kc :
        km = kc.keymaps.new(name ='3D View' , space_type = 'VIEW_3D')
        kmi = km.keymap_items.new("gpencil.dvswitch", type = 'QUOTE', value = 'PRESS')
        addon_keymaps.append((km, kmi))

'''

def unregister():
    for cls in reversed(classes) :
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.dv_data
    
    # handle the keymap
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()
    
    
 
if __name__ == "__main__":
    register()


